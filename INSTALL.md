# Installation Instructions

## Simple Install

Execute `sudo make install`. This will do the following:

- create a python virtual environment in `/opt/yt/venv`
- install the software in `/opt/yt/app`
- copy config files to `/etc/yt`

You are then expected to edit `/etc/yt/config.ini` and set `[webhooks]` ->
`public_uri` to the domain (and port) you're running this on. Feel free to
change the `welcome_message` or disable unneeded `modules`.

By default, the web frontend will run unencrypted on port 8080; this can be
changed in `/etc/yt/gunicorn-frontend-config.py`. Alternatively, run it from
behind a reverse-proxy.

Finally, run `sudo make finish` to enable and start the systemd units and start
populating the guest feed.

A user `admin` is created, with the password `admin`. You should change that!
Either use this user for yourself, or create a new user on the admin's account
settings page.

## Advanced: customizing the installation

The following is more a list of possibilities than one of recommendations. It's
best to just stick to the normal installation with `make install`.

The systemd unit files can be modified to install the software, virtualenv
and/or config files into different locations.

- `WorkingDirectory` should point to the parent directory of `app/`;
- `Environment=PATH` to the virtualenv's `bin/`;
- `Environment=YT_CONFIG` to the `config.ini`.
- The gunicorn config can be set by modifying `ExecStart`.

To allow using classic cronjobs to run the websub/rss jobs, `utils.py` is a
polyglot file that when executed re-launches itself in the virtual environment.
For this to work, the venv must be placed in `venv/` next to the `app/`
directory.

**Do not start the frontend/timers/cronjobs before the database is in place!**

To create the database and tables, simply issue:

    sqlite3 subscriptions.sqlite < config/setup.sql

If you do not want to use the admin account automatically created, one can be
created manually. Note that the password is written into the database in plain
text and should be changed from the web UI after logging in.

	USERN=your_name
	PASSWD='plain$$your_password'  # 'plain$$' prefixed
	TOKEN=$(head -c16 </dev/urandom|base64|tr /+ _-|tr -d =)
	echo "INSERT INTO users(name, password, token, is_admin) VALUES ('$USERN','$PASSWD','$TOKEN', 1)" |
		sqlite3 subscriptions.sqlite

The homepage will show the guest user's subscription feed. As you cannot log in
as guest, you will have to populate this feed by INSERTing directly into the
datbase. The README's *Advanced Topics* section has information on this. Or you
can use the provided list:

	sqlite3 subscriptions.sqlite < config/guest.sql

You can do the same for your own subscriptions, or use the *Subscription
Manager* in the footer of the main page to subscribe to channels manually.

Before starting the frontend, you should run the cronjobs/timers once.
`update.py pull` downloads a back catalog of subscriptions; `update.py websub`
will register for automatic push-updated on new videos. Note that this will
take a long while, as we only do 1 request per minute to Google's RSS endpoints.

    systemctl start subscriptions-update@pull.timer
    systemctl start subscriptions-update@websub.timer

If you don't want to use a reverse-proxy, [gunicorn can be configured] to
directly serv traffic over TLS by setting a certificate (`certfile`) and
private key (`keyfile`). However, it won't listen on http/tcp to redirect
traffic to https/tcp. For this, a very basic HTTP-to-HTTPS redirect "server" is
available as `subscriptions-port80.service` (requires netcat(1)); it needs the
domain name of your instance in `Environment=DOMAIN=`.

[gunicorn can be configured]: https://docs.gunicorn.org/en/stable/settings.html

It is possible to run the `webhooks` blueprint as a standalone flask app. This
can be useful to still receive new video notifications, even while the frontend
is offline. Copy `subscriptions-frontend.service` and change `ExecStart` like
so:

    ExecStart=/usr/bin/env gunicorn --config /etc/yt/gunicorn-webhooks-config.py 'app.webhooks:app()'

In `config.ini`, update `[webhooks] public_uri` and optionally remove
`webhooks` from the `[frontend] modules` list (it will be unused, but shouldn't
hurt). Configure `gunicorn-webhooks-config.py` to listen on a seperate port;
TLS is optional.

All `[frontend] modules` except `youtube` are optional and can be disabled if
desired. Let's look at what each of them does:

- `youtube`: allows watching videos and handles the subscription feed. channel
  and playlist pages only shows the last 15 uploads.
- `browse`: implements search, extends the channel/playlist pages to full
  functionality.
- `webhooks`: receives notifications from Youtube about new uploads. If
  disabled, the subscription feed will be updated from rss feeds once per day.
- `proxy`: livestreams and geolocked videos must be proxied to work. If
  disabled, an error is shown when such a video is requested.
- `reddit`: shows videos uploaded to specific subreddits (mainly, /r/videos).
  Logged in users can configure their subscribed subreddits.
