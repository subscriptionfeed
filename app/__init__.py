import base64
import secrets
import importlib
from flask import Flask

from .common.common import *
from .common.user import init_login

app = Flask(__name__)
app.secret_key = base64.b64decode(cf['frontend'].get('secret_key','')) or \
    secrets.token_bytes(16) # development fallback; CSRF/cookies won't persist.
init_login(app)

for name in cf['frontend']['modules'].split(','):
    blueprint = importlib.import_module('.'+name, __name__)
    app.register_blueprint(blueprint.frontend)

# TODO: should this go somewhere else?
# This error handler logs requests to external apis, and POST data. this makes debugging of api responses easier, as the request can be reconstructed and replayed.
from flask import g, request
from werkzeug.exceptions import InternalServerError
@app.errorhandler(InternalServerError)
def log_errors(e):
    if request.method == "POST":
        app.logger.error(request.data)
    if 'api_requests' in g:
        app.logger.error(g.api_requests)
    return e

from .common import anticsrf
anticsrf.init(app)
