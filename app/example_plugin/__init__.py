"""
This is an example Blueprint to base your plugin off of.
to use it, add the blueprint's name (e.g. "example_plugin") in
config/config.ini, under the [frontend] section, to the modules key (csv).
first-named blueprints take precidence (which allows overwriting existing
routes from other blueprints).
"""
from flask import Blueprint, g, url_for
from flask_login import current_user

from ..common.common import *

frontend = Blueprint(__name__.rpartition('.')[2], __name__)

@frontend.route('/hello/')
@frontend.route('/hello/<name>')
def index(name=None):
    token = getattr(current_user, 'token', 'guest') # 'standard' way to get user info when log-in is not required
    # assume there's some other /hello/world endpoint to handle this special case:
    if name == "world":
        return fallback_route(name)
    return f"hello, {name or 'you'} from user {token}!" # note: token is usually not a username, but a random string

@frontend.before_app_request
def inject_header_item(): # makes a submodule accessible.
    if not 'header_items' in g:
        g.header_items = []
    g.header_items.append({
        'name': 'hello', # link text
        'url': url_for(frontend.name+'.index'), # must use absolute endpoint name!
        'parent': frontend.name, # required, so it gets printed boldface when blueprint is active
        'priority': 1, # higher priority, further left.
    })
