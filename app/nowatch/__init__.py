"""
This blueprint overrides /watch and provides links where to view the video instead.
"""
from flask import Blueprint, request, render_template
from werkzeug.exceptions import BadRequest

frontend = Blueprint(__name__.rpartition('.')[2], __name__, template_folder='templates')

@frontend.route('/watch')
def watch():
    if not 'v' in request.args:
        raise BadRequest("missing video id")
    if len(request.args.get('v')) != 11:
        raise BadRequest("malformed video id")

    return render_template('nowatch.html.j2', video_id = request.args.get('v'))
