#!/bin/sh
''':'
. "`dirname "$0"`/../venv/bin/activate"
# uncomment if you aren't using /etc/yt/config.ini:
#export YT_CONFIG=/opt/yt/config/config.ini

exec python3 "$0" "$@"
':'''

if __package__ is None: # called as ./__main__.py
    import sys, os
    sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from app import app

if __name__ == '__main__':
    import sys
    if '--routes' in sys.argv:
        for rule in app.url_map.iter_rules():
            print(f"{rule.endpoint}\t{rule.rule}")
        sys.exit(0)
    app.run(debug=True, port=8000, host="0.0.0.0")
