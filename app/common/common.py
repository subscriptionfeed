import os
import re
import json
import base64
import sqlite3
import requests
import hmac, hashlib
import requests_cache
import dateutil.parser
from xml.etree import ElementTree
from configparser import ConfigParser
from datetime import datetime, timezone
from urllib.parse import parse_qs, urlparse

cf = ConfigParser()
config_filename = os.environ.get('YT_CONFIG', '/etc/yt/config.ini')
cf.read(config_filename)
if not 'global' in cf: # todo: full config check
    raise Exception("Configuration file not found or empty")

# Note: currently expiring after 10 minutes. googlevideo-urls are valid for 5h59m, but this makes reddit very stale and premiere videos won't start.  TODO: exipre when video is livestream/premiere/etc
requests_cache.install_cache(backend='memory', expire_after=10*60, allowable_codes=(200,), allowable_methods=('GET', 'HEAD', 'POST'))

# Note: requests-cache doesn't use redis expiry, so we need this in all backends:
# https://github.com/reclosedev/requests-cache/issues/58#issuecomment-164537971
# TODO: only run for long-running processes, i.e. the frontend
from threading import Timer
def purge_cache(sec):
    requests_cache.remove_expired_responses()
    t = Timer(sec, purge_cache, args=(sec,))
    t.setDaemon(True)
    t.start()
purge_cache(10*60)

# for debugging purposes, monkey patch requests session to store each requests-request in a flask-request's g object (url and response). we can then use a flask error_handler to include the request data in the error log.
# since we also call config from outside the flask appcontext, it is wrapped in a try-catch block.
from flask import g
import requests
from requests import Session as OriginalSession
class _NSASession(OriginalSession):
    def request(self, method, url, params=None, data=None, json=None, **kwargs):
        response = super(_NSASession, self).request(
            method, url, params=params, data=data, json=json, **kwargs
            )
        try:
            if 'api_requests' not in g:
                g.api_requests = []
            g.api_requests.append((url, params, json, response.text))
        except RuntimeError: pass # not within flask (e.g. utils.py)
        return response
requests.Session = requests.sessions.Session = _NSASession

class G:
    """
    null-coalescing version of dict.get() that also works on lists.

    the | operator is overloaded to achieve similar looking code to jq(1) filters.
    the first found key is used: dict(foo=1)|G('bar','foo') returns 1.
    """
    def __init__(self, *keys):
        self.keys = keys
    def __ror__(self, other):
        for key in self.keys:
            try:    return other[key]
            except: continue
        return None
    class _Text:
        """ parses youtube's .runs[].text and .simpleText variants """
        def __ror__(self, other): # Note: only returning runs[0], not concat'ing all!
            return other|G('simpleText') or other|G('runs')|G(0)|G('text')
    text = _Text()

def fetch_xml(feed_type, feed_id):
    # TODO: handle requests.exceptions.ConnectionError
    r = requests.get("https://www.youtube.com/feeds/videos.xml", {
        feed_type: feed_id,
    })
    if not r.ok:
        return None

    return r.content

def parse_xml(xmldata):
    ns = {
        'atom':"http://www.w3.org/2005/Atom",
        'yt': "http://www.youtube.com/xml/schemas/2015",
        'media':"http://search.yahoo.com/mrss/",
        'at': "http://purl.org/atompub/tombstones/1.0",
    }

    feed = ElementTree.fromstring(xmldata)

    if feed.find('at:deleted-entry',ns):
        del_entry = feed.find('at:deleted-entry',ns)
        del_author = del_entry.find('at:by',ns)
        _, _, vid = del_entry.get('ref').rpartition(':')
        _, _, channel_id = del_author.find('atom:uri',ns).text.rpartition('/')
        author = del_author.find('atom:name',ns).text
        when = del_entry.get('when')
        entry = [{
            'deleted': True,
            'video_id': vid,
            'channel_id': channel_id,
            'author': author,
            'timestamp': when,
        }]
        return None, None, entry, None, None

    title = feed.find('atom:title',ns).text
    author = feed.find('atom:author/atom:name',ns).text \
        if feed.find('atom:author',ns) else None
    # for /user/<> endpoint: find out UC-id:
    # for playlists: this is who created the playlist:
    try:   channel_id = feed.find('yt:channelId',ns).text
    except:channel_id = None
    # for pullsub: if this exists, we're looking at a playlist:
    try:   playlist_id = feed.find('yt:playlistId',ns).text
    except:playlist_id = None
    videos = []
    for entry in feed.findall('atom:entry',ns):
        videos.append({
            'video_id': entry.find('yt:videoId',ns).text,
            'title': entry.find('atom:title',ns).text,
            'published': entry.find('atom:published',ns).text,
            'channel_id': entry.find('yt:channelId',ns).text,
            'author': entry.find('atom:author',ns).find('atom:name',ns).text,
            # extra fields for pull_subs/webhook:
            'updated': entry.find('atom:updated',ns).text,
        })

    return title, author, videos, channel_id, playlist_id

def update_channel(db, xmldata, from_webhook=False):
    if not xmldata: return False

    # Note: websub does not return global author, hence taking from first video
    title, author, videos, channel, playlist = parse_xml(xmldata)

    c = db.cursor()
    for i, video in enumerate(videos):
        if video.get('deleted'):
            # Note: Deletion events are not just fired for actual deletions,
            # but also for unlisting videos and livestreams that just ended
            # (even postLiveDVR ones). Hence, we don't follow it.
            flask_logger(f"ignoring deleted/unlisted video or ended livestream {video['video_id']} by {video['channel_id']} ({video['author']})")
            # NOTE: the following table only stores the last time a video was deleted.
            # TODO: since we rely on the videos table to connect video_id to channel_id, being gracious with LEFT JOIN doesn't help.
            #       would need to store stubs into videos table (video_id,channel_id,title="",published=unixepoch) but we then wouldn't be able to update published or insert length/livestream/premiere/shorts/etc when video comes back
            #       updating the latter would generally be good, so we can gradually recover from minimal metadata only subscriptions (only feed's xml data, no get_video_info; e.g. fetched too early or fetched during ip ban)
            c.execute("INSERT OR REPLACE INTO deletions (video_id, timestamp) VALUES (?, ?)", (video['video_id'], video['timestamp']))
            c.execute("INSERT OR REPLACE INTO channels (id, name) VALUES (?, ?)", (video['channel_id'], video['author']))
            break

        c.execute("SELECT 1 FROM videos WHERE id=?",(video['video_id'],))
        new_video = len(c.fetchall()) < 1
        if new_video:
            # TODO: call store_video_metadata(video_id) here instead and pass video-fallback-metadata to it
            _, _, meta, _, _ = get_video_info(video['video_id'], metaOnly=True)
            # The 'published' timestamp sent in websub POSTs are often wrong (e.g.:
            # video gets uploaded as unlisted on day A and set to public on day B;
            # the webhook is sent on day B, but 'published' says A. The video
            # therefore looks like it's just an update to an older video).
            # g_v_i gives is the date the video was published to viewers, so we
            # prefer that. But since g_v_i only returns the date without time,
            # we still use xmlfeed's date if it's the same date.
            published = dateutil.parser.parse(video['published'])
            length = None
            livestream = None
            premiere = None
            shorts = None
            if meta:
                meta = video_metadata(meta)
                published2 = dateutil.parser.parse(meta['published'])
                if published < published2: # g_v_i date is more accurate:
                    published = published2
                length = meta['length']
                livestream = meta['livestream']
                premiere = meta['premiere']
                shorts = meta['shorts']

            now = datetime.now(timezone.utc)

            # we pretend that all videos uploaded this week were uploaded just
            # now, so the user sees it at the top of the feed, and it doesn't
            # get inserted somewhere further down.
            if (now - published).days < 7:
                timestamp = now
            else:#, it's just an update to an older video.
                timestamp = published

            c.execute("""
                INSERT OR IGNORE INTO videos
                    (id, channel_id, title, length, livestream, premiere, shorts, published, crawled)
                VALUES (?, ?, ?, ?, ?, ?, ?, datetime(?), datetime(?))
            """, (
                video['video_id'],
                video['channel_id'],
                video['title'],
                length,
                livestream,
                premiere,
                shorts,
                published,
                timestamp
            ))
        else:
            #TODO: if length==livestream==premiere==shorts==NULL -> metadata is missing -> retry fetching these fields
            # (technically it should be enough to check any single one of the booleans. livestream is always 0/1; premiere needs to be backfilled; shorts is only null if we have neither length nor aspect (i.e. no metadata))
            # update video title (everything else can't change)
            c.execute("""
                UPDATE OR IGNORE videos
                    SET title = ?
                    WHERE id = ?
            """, (
                video['title'],
                video['video_id'],
            ))

        # for channels, this is obviously always the same, but playlists can
        # consist of videos from different channels:
        if i == 0 or playlist:
            c.execute("""
                INSERT OR REPLACE INTO channels (id, name)
                                VALUES (?, ?)
            """, (video['channel_id'], video['author']))

        # keep track of which videos are in a playlist, so we can show the user
        # why a video is in their feed:
        if playlist:
            c.execute("""
                INSERT OR IGNORE INTO playlist_videos (video_id, playlist_id)
                               VALUES (?, ?)
            """, (video['video_id'], playlist))

    if playlist and not from_webhook: # Note: playlists can't get updated via websub
        c.execute("""
            INSERT OR REPLACE INTO playlists (id, name, author)
                            VALUES (?, ?, ?)
            """, (playlist, title, channel))
        c.execute("""
            INSERT OR REPLACE INTO channels (id, name)
                            VALUES (?, ?)
        """, (channel, author))

    db.commit()

    return True

def is_agegated(metadata):
    playabilityStatus = metadata['playabilityStatus']
    return bool(
        playabilityStatus.get("status") == "CONTENT_CHECK_REQUIRED"
        or playabilityStatus.get("desktopLegacyAgeGateReason")
    )

def get_video_info(video_id, *, metaOnly=False, _agegate_bypass=False):
    """
    returns: best-quality muxed video stream, stream map, player_response, error-type/mesage
    error types: player, malformed, livestream, geolocked, agegated, no-url, exhausted
    """
    player_error, metadata = None, None # for 'exhausted'
    today = datetime.now(timezone.utc).strftime("%Y%m%d")
    key = "AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8" if metaOnly or _agegate_bypass else "AIzaSyA8eiZmM1FaDVjRy-df2KTyQ_vz_yYM39w"
    # ANDROID returns streams that are not throttled or cipher-scambled, but less metadata than WEB.
    # TVHTML5* returns throttled and possibly ciphered streams, but bypasses age-gate. atm, we don't decipher them.
    # TODO: unscramble TVHTML5* streams (especially &n= throttling)
    client = {
        (False, False): { 'clientName': 'MEDIA_CONNECT_FRONTEND',         'clientVersion': '0.1'                                    },
        (False, True):  { 'clientName': 'TVHTML5_SIMPLY_EMBEDDED_PLAYER', 'clientVersion': '2.0'                                    },
        (True, False):  { 'clientName': 'WEB',                            'clientVersion':f'2.{today}.01.01'                        },
    }[(metaOnly, _agegate_bypass)]
    r = requests.post("https://youtubei.googleapis.com/youtubei/v1/player", json={
        'videoId': video_id,
        'context': {
            'client': {
                'gl': 'US',
                'hl': 'en',
                **client,
            },
            'thirdParty': {'embedUrl': 'https://www.youtube.com/'}
        },
	"racyCheckOk": True, # seems to do nothing, cargo-culted
	"contentCheckOk": True, # fix "This video may be inappropriate for some users."
    }, headers={"User-Agent": "Mozilla/5.0"})

    if not r or r.status_code == 429:
        return None, None, None, 'banned', 'possible IP ban'

    metadata = r.json()
    if "error" in metadata:
        return None, None, metadata, "malformed", metadata.get("error",{}).get("message","")
    real_vid = metadata.get("videoDetails", {}).get("videoId")
    if video_id != real_vid and real_vid in ("M5t4UHllkUM", "aQvGIIdgFDM"):
        # youtube redirected us to a clip called "Video Not Available". indicates a long-term ip ban.
        return None, None, {}, "banned", "instance is probably ip banned"
    playabilityStatus = metadata['playabilityStatus']['status']
    if playabilityStatus != "OK":
        playabilityReason = metadata['playabilityStatus'].get('reason',
                '//'.join(metadata['playabilityStatus'].get('messages',[])))
        player_error = f"{playabilityStatus}: {playabilityReason}"
        if (is_agegated(metadata)
            and not metaOnly # only need metadata (e.g. called from pubsubhubbub)
            and not _agegate_bypass
        ):
            _, _, metadata_embed, error_embed, errormsg_embed = get_video_info(video_id, _agegate_bypass=True)
            if error_embed == "player": # agegate bypass failed?
                return None, None, metadata, 'agegated', player_error
            elif not error_embed or error_embed in ('livestream','geolocked','scrambled', 'throttled'):
                metadata = metadata_embed
            else:
                return None, None, metadata, error_embed, errormsg_embed
        else:
            # without videoDetails, there's only the error message
            maybe_metadata = metadata if 'videoDetails' in metadata else None
            return None, None, maybe_metadata, 'player', player_error

    # livestreams have no adaptive/muxed formats:
    is_live = metadata['videoDetails'].get('isLive', False)

    if not 'formats' in metadata['streamingData'] and not is_live:
        return None, None, metadata, 'no-url', player_error

    formats = metadata['streamingData'].get('formats',[])
    adaptive = metadata['streamingData'].get('adaptiveFormats',[])
    stream_map = {
        'adaptive_video': [a for a in adaptive if a['mimeType'].startswith('video/')],
        'adaptive_audio': [a for a in adaptive if a['mimeType'].startswith('audio/')],
        'muxed': formats,
        'hlsManifestUrl': metadata['streamingData'].get('hlsManifestUrl'),
    }

    try:
        url = sorted(formats, key=lambda k: k['height'], reverse=True)[0]['url']

        query = parse_qs(urlparse(url).query)
        # ip-locked videos can be recovered if the proxy module is loaded:
        is_geolocked = 'gcr' in query
        # "n-signature" requires javascript descrambling (not implemented):
        is_throttled = 'ns' in query
    except:
        url = None
        is_geolocked = False
        is_throttled = False

    is_drm = formats and 'signatureCipher' in formats[0]

    nonfatal = 'livestream' if is_live \
        else 'geolocked' if is_geolocked \
        else 'scrambled' if is_drm \
        else 'throttled' if is_throttled \
        else None

    return url, stream_map, metadata, nonfatal, None

def video_metadata(metadata):
    if not metadata:
        return {}

    meta1 = metadata['videoDetails']
    # With ANDROID player API, we don't get microformat => no publishDate!
    meta2 = metadata.get('microformat',{}).get('playerMicroformatRenderer',{})

    # sometimes, we receive the notification so early that the length is not
    # yet populated. Nothing we can do about it. meta1 and meta2 use a
    # different rounding strategy, meta2 is sometimes (incorrectly) 1s longer.
    length = int(meta1.get('lengthSeconds',0)) or int(meta2.get('lengthSeconds',0)) or None

    views = int(meta1['viewCount']) if 'viewCount' in meta1 else None

    scheduled_time = metadata.get('playabilityStatus',{}) \
        .get('liveStreamability',{}).get('liveStreamabilityRenderer',{}) \
        .get('offlineSlate',{}).get('liveStreamOfflineSlateRenderer',{}) \
        .get('scheduledStartTime')
    if scheduled_time:
        scheduled_time = datetime.fromtimestamp(int(scheduled_time)) \
            .strftime("%Y-%m-%dT%H:%M:%SZ")
    published_at = (
        meta2.get('liveBroadcastDetails',{}) .get('startTimestamp') or
        scheduled_time or
        meta2.get('publishDate','1970-01-01T00:00:00Z')
    )

    # the actual video streams have exact information:
    # Note that we use x:1 (cinema style) aspect ratios, omitting the ':1' part.
    try:
        sd = metadata['streamingData']
        some_stream = (sd.get('adaptiveFormats',[]) + sd.get('formats',[]))[0]
        aspect_ratio =  some_stream['width'] / some_stream['height']
    # if that's unavailable (e.g. on livestreams), fall back to 16:9 (later)
    except:
        aspect_ratio = None

    is_livestream = meta1['isLiveContent']
    is_premiere = meta1.get('isUpcoming', False) and not is_livestream
    # shorts are <= 60 seconds and vertical or square. they can't be premiere
    # or livestreams. if we were unable to determine it, we set it to None.
    is_short = (
        True if (length or 61) <= 60 and (aspect_ratio or 2) <= 1 else
        False if (length or 0) > 60 or (aspect_ratio or 0) > 1 else
        None if not is_premiere and not is_livestream else False
    )

    # Note: 'premiere' videos have livestream=False and published= will be the
    # start of the premiere.
    return {
        'title': meta1['title'],
        'author': meta1['author'],
        'channel_id': meta1['channelId'],
        'published': published_at,
        'views': views,
        'length': length,
        'aspect': aspect_ratio or 16/9,
        'livestream': is_livestream,
        'premiere': is_premiere,
        'shorts': is_short,
    }

def mkthumbs(thumbs):
    output = {str(e['height']): e['url'] for e in thumbs}
    largest=next(iter(sorted(output.keys(),reverse=True,key=int)),None)
    return {**output, 'largest': largest}

def store_video_metadata(video_id):
    # check if we know about it, and if not, fetch and store video metadata
    with sqlite3.connect(cf['global']['database']) as conn:
        c = conn.cursor()
        c.execute("SELECT 1 from videos where id = ?", (video_id,))
        new_video = len(c.fetchall()) < 1
        if new_video:
            _, _, meta, _, _ = get_video_info(video_id, metaOnly=True)
            if meta:
                meta = video_metadata(meta)
                c.execute("""
                    INSERT OR IGNORE INTO videos (id, channel_id, title, length, livestream, premiere, shorts, published, crawled)
                                   VALUES (?, ?, ?, ?, ?, ?, ?, datetime(?), datetime(?))
                """, (
                    video_id,
                    meta['channel_id'],
                    meta['title'],
                    meta['length'],
                    meta['livestream'],
                    meta['premiere'],
                    meta['shorts'],
                    meta['published'],
                    meta['published'],
                ))
                c.execute("""
                    INSERT OR REPLACE INTO channels (id, name)
                                    VALUES (?, ?)
                """, (meta['channel_id'], meta['author']))

def fetch_video_flags(token, video_ids):
    with sqlite3.connect(cf['global']['database']) as conn:
        c = conn.cursor()
        c.execute("""
            SELECT video_id,display
              FROM flags
             WHERE user = ?
               AND display IS NOT NULL
               AND video_id IN ({})
               -- AND display = 'pinned'
        """.format(",".join(["?"]*len(video_ids))), (token,*video_ids))
        flags = c.fetchall()
        pinned = [video for video,disp in flags if disp == 'pinned']
        hidden = [video for video,disp in flags if disp == 'hidden']

        return pinned, hidden

def apply_video_flags(token, rows, settings={}):
    video_ids = [card['content']['video_id'] for card in rows if 'video_id' in card['content']]
    pinned, hidden = fetch_video_flags(token, video_ids)
    noshorts = settings.get('noshorts') or False
    return sorted([
        {'type':v['type'], 'content':{**v['content'], 'pinned': v['content']['video_id'] in pinned if 'video_id' in v['content'] else False}}
        for v in rows
        if (
            'video_id' not in v['content'] or v['content']['video_id'] not in hidden
        ) and (
            not (noshorts and v['content'].get('shorts'))
        )
    ], key=lambda v:v['content']['pinned'], reverse=True)

from werkzeug.exceptions import NotFound
class NoFallbackException(NotFound): pass
def fallback_route(*args, **kwargs): # TODO: worthy as a flask-extension?
    """
    finds the next route that matches the current url rule, and executes it.
    args, kwargs: pass all arguments of the current route
    """
    from flask import current_app, request, g

    # build a list of endpoints that match the current request's url rule:
    matching = [
        rule.endpoint
        for rule in current_app.url_map.iter_rules()
        if rule.rule == request.url_rule.rule
    ]
    current = matching.index(request.endpoint)

    # since we can't change request.endpoint, we always get the original
    # endpoint back. so for repeated fall throughs, we use the g object to
    # increment how often we want to fall through.
    if not '_fallback_next' in g:
        g._fallback_next = 0
    g._fallback_next += 1

    next_ep = current + g._fallback_next

    if next_ep < len(matching):
        return current_app.view_functions[matching[next_ep]](*args, **kwargs)
    else:
        raise NoFallbackException

def websub_url_hmac(key, feed_id, timestamp, nonce):
    """ generate sha1 hmac, as required by websub/pubsubhubbub """
    sig_input = f"{feed_id}:{timestamp}:{nonce}".encode('ascii')
    return hmac.new(key.encode('ascii'), sig_input, hashlib.sha1).hexdigest()

def websub_body_hmac(key, body):
    return hmac.new(key.encode('ascii'), body, hashlib.sha1).hexdigest()

def flask_logger(msg, level="warning"):
    level = dict(
        CRITICAL=50,
        ERROR=40,
        WARNING=30,
        INFO=20,
        DEBUG=10,
        NOTSET=0,
    ).get(level.upper(), 0)
    try:
        from flask import current_app
        current_app.logger.log(level, msg)
    except:
        pass

def log_unknown_card(data):
    import json
    try:
        from flask import request
        source = request.url
    except: source = "unknown"
    with open("/tmp/innertube.err", "a", encoding="utf-8", errors="backslashreplace") as f:
        f.write(f"\n/***** {source} *****/\n")
        json.dump(data, f, indent=2)
