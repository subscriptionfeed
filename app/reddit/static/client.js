function template(tpl, replacements) {
	function safetify(s) {
	    // decodes and re-encodes html entities
	    let e = document.createElement("textarea");
	    e.innerHTML = s;
	    e.innerText = e.value;
	    return e.innerHTML.replaceAll('"','&quot;').replaceAll("'","&apos;");
	}

	for (let key in replacements) {
		tpl = tpl.replaceAll("{{"+key+"}}", safetify(replacements[key]));
	}
	return tpl;
}

function load_page(subreddits, {sorted_by='hot', time='', limit=36, count='', before='', after=''}) {

	let multireddit = subreddits.join("+");
	let query = new URLSearchParams({
		count, before, after, limit,
		t: time, // hour,week,month,year,all
	}).toString();

	fetch_jsonp(`https://www.reddit.com/r/${multireddit}/${sorted_by}.json?${query}`).then(json => {
		for (entry of json.data.children) {
			let e = entry.data;
			if (e.score == 0) continue; /*more downvotes than upvotes, ignore*/
			if (!['youtube.com', 'youtu.be', 'youtube-nocookie.com'].includes(e.domain)) continue;
			let match = e.url.match(/^https?:\/\/(?:www.|m.)?(?:youtube.com\/watch\?(?:.*&amp;)?v=|youtu.be\/|youtube(?:-nocookie)?.com\/(?:embed|shorts|live)\/|youtube.com\/)([-_0-9A-Za-z]+)(?:[?&#]t=([0-9hms:]+))?/);
			if (!match) continue;
			let video_id = match[1], timestamp = match[2], maybe_length=null;
			match = e.title.match(/.*[\[(](?:00:)?(\d\d?(?::\d\d){1,2})[\])]/);
			if (match) {
				maybe_length = match[1];
                		// 20:59:00 => 20:59 (we're assuming no video is >10h)
				maybe_length = maybe_length.replace(/([1-9]\d:\d\d):00/, "$1")
			}
			document.querySelector('#cards').innerHTML += template(window.card.innerHTML, {
				karma: e.score,
				title: e.title,
				url: e.permalink,
				videoid: video_id,
				length: maybe_length||'',
				timestamp: timestamp||'',
				comments: e.num_comments,
				subreddit: e.subreddit,
			});
		}
		document.querySelector('#cards').innerHTML += window.dummycards.innerHTML;
		document.querySelector('.pagination-container').innerHTML = window.pagination.innerHTML
			.replaceAll(/%7B%7Bafter%7D%7D/g, json.data.after);
	}).catch(e=>{
		console.error(e);
		document.querySelector('#cards').innerHTML = `
			<ul class=flashes><li class="error">Loading reddit content failed. Make sure requests to reddit.com are not blocked by your browser or an addon.</li></ul>
		`;
	});
}

/* the following code has been adapted from https://xkcd.wtf (AGPLv3) */
let __fetch_jsonp_index = 0;
let abort = new AbortController();
function fetch_jsonp(url, cbparam = 'jsonp') {
	abort.abort(); //cancel previous request
	abort = new AbortController(); //re-init aborter
	return new Promise((resolve, reject) => {
		let tmp = '__fetch_jsonp_'+__fetch_jsonp_index++;
		let s = document.createElement("script");
		s.src = url + '&' + cbparam + '=' + tmp;
		s.onerror   = err => reject(err);
		window[tmp] = json => resolve(json);
		abort.signal.addEventListener('abort', ()=> {
			s.src="";
			reject(new DOMException('Aborted', 'AbortError'))
		});
		document.body.appendChild(s);
	});
}
