.PHONY: all install finish

all:
	@echo 'issue "sudo make install" to install subscriptionfeed'

install:
	install -d /opt/yt
	cp -a app /opt/yt
	install -Dt /etc/yt config/config.ini config/gunicorn-frontend-config.py
	install -Dt /etc/systemd/system config/subscriptions-frontend.service
	install -Dt /etc/systemd/system config/subscriptions-port80.service
	install -Dt /etc/systemd/system config/subscriptions-update@.service
	install -Dt /etc/systemd/system config/subscriptions-update@.timer
	systemctl daemon-reload
	python3 -m venv /opt/yt/venv
	/opt/yt/venv/bin/pip install -r config/requirements.txt
	sqlite3 /opt/yt/subscriptions.sqlite < config/setup.sql
	sqlite3 /opt/yt/subscriptions.sqlite < config/guest.sql
	# admin user with default password -- must be changed on first login!
	./config/create-admin.sh /opt/yt/subscriptions.sqlite
	# automatically generate hmac secret keys for config.ini:
	sed -i -e '/^secret_key/s@$$@'"`head -c32 /dev/urandom | base64`"'@' -e '/^hmac_key/s@x$$@'"`tr -dc '[:print:]' </dev/urandom|tr -d @ | head -c 32`"'@' /etc/yt/config.ini
	#
	@echo '###################################################################'
	@echo '# installation finished!                                          #'
	@echo '# edit /etc/yt/config.ini and /etc/yt/gunicorn-frontend-config.py #'
	@echo '# then run "sudo make finish" to start the server                 #'
	@echo '###################################################################'

finish:
	systemctl enable subscriptions-frontend.service
	systemctl enable subscriptions-update@websub.timer
	systemctl enable subscriptions-update@pull.timer
	systemctl start subscriptions-frontend.service
	systemctl start subscriptions-update@websub.timer
	systemctl start subscriptions-update@pull.timer
	# one-shot to initialize db with guest subscriptions in the background:
	systemctl start subscriptions-update@pull.service &
	#
	@echo '###################################################################'
	@echo '# the software is now running.                                    #'
	@echo '# a user named "admin" with password "admin" has been created.    #'
	@echo '# log in, then change the password by clicking on the username!   #'
	@echo '###################################################################'
