# Unnamed Youtube Frontend

**Contributors wanted!** Please send me an email (see commit log) or message `girst` on irc.libera.chat/#invidious

Test Instance: [https://subscriptions.gir.st/](https://subscriptions.gir.st/)

# Installation

see INSTALL file

# Features

 - subscribe to channels and playlists
 - raw video/audio mode (downloadable)
 - automatically skips over sponsored video segments (javascript required)
 - proxies georestricted videos
 - subreddit viewer
 - extremely leight weight: requires << 100MB RAM or disk space (multi-user test instance runs on bottom-of-the-barrel 10$/y VPS)

# Non-Features

 - no algorithmically determined video recommendations: Find additional content through Youtube search, endcards or Reddit
 - no youtube comments

# TODOs

If you can program in Python or know HTML/CSS, get in touch!

## important/big features

 - Find a recognizable/googleable/duckable name  
   send suggestions!
 - 1080p support  
   Problem: must mux video and audio streams client side; requiring javascript (non-MediaSource solution to avoid CORS)
 - /watch endpoint should allow continue playing playlist (&list=PL...&index=x)
 - support for autogenerated channels  
   Problem: require different protobuf structures; cannot differentiate from normal channels  
   Samples: UCbqBqmG-EXTUsp2v4SHGDRg, UCuVPpxrm2VAgpH3Ktln4HXg, UClgRkhTL3_hImCAmdLfDE4g

## users' wishlist

 - Tag javascripts with LibreJS  
   Problem: SPDX not supported; LibreJS doesn't define a AGPLv3-only identifier
 - allow proxying all thumbnails/googlevideo URLs  
   Note: should be disableable in the config; only touching proxy/ blueprint

## internal stuff

 - moar internal and public documentation
 - when `get_video_info` returns a 403ing video url, remove from cache and retry  
   Problem: requires an additional HEAD to googlevideo.com; must check
 - immediately subscribe  
   asynchronically call update-subs and pull-subs on subscribing, iff necessary
 - remove deleted videos from subscription feed  
   Note: when a deletion websub comes in, check with `get_video_info`  
   Note: for livestreams, check postlivedvr and queue deletion after it ended
 - cleanups:
   - split up common/common.py; move stuff into plugins
   - abstract database access
   - proper task queue for refesh-cipher, pull-subs, update-websub
   - fix all TODOs/XXXs
   - welcome message: maybe make dismissable, show on all pages?


# Advanced Topics

## Extending Unnamed Youtube Frontend

UYtF is trivially extensible using [Flask Blueprints]. Just drop a new
blueprint in `app/` and register it in the `[frontend]modules` section of
`config.ini`. The Blueprint inside a module is expected to be named `frontend`.
An `example_blueprint` is provided, which shows off how to use `fallback_route`
and fetching the user token on guest-accessible routes.

The default `youtube` blueprint provides unblockable but minimal versions of
the /channel and /playlist endpoints. A custom blueprint (or, as an example,
the `browse` blueprint) can overwrite existing routes by being listed before
the one to be overridden in `config.ini`.
A blueprint may also delegate a request to another (lower precedence) blueprint
using `return fallback_route(*args, **kwargs)`. This can be useful if the
endpoint might not be as reliable.

[Flask Blueprints]: https://flask.palletsprojects.com/en/1.1.x/blueprints/

## Proxying videos

This is useful to watch videos that are IP-locked. add the `proxy` blueprint to
the `[frontend]modules` list in `config.ini` to enable (disabled by default, as
it is resource-intensive, blocks a gunicorn thread and hasn't been tested
w.r.t. googlebanning).

## Guest User

The guest user doesn't really exist; you can't log in as guest, and all guest
views are read-only. To modify the shown subscription feed, you'll have to
modify the database manually. To add a single channel, issuing
`INSERT INTO subscriptions (user, channel_id) VALUES ("guest", "UC...")`. To
load many at once, prepare a file (guest.csv) of channel ids
that looks like this:

    UCxxxxxxxxxxxxxxxxxxxxxxxx,guest,channel

Then from the `sqlite3` console, issue:
    .mode csv
    .import guest.csv subscriptions

## Running modules on different Hosts

The `webhooks` and `proxy` modules support being ran standalone on a different
machine. For this, remove them from `config.ini`'s `[frontend]modules` and
start them using gunicorn by replacing `app:app` with (e.g.)
`app.webhooks:app()` (the parenthesis are required, as those use the factory
pattern to not uselessly instantiate the Flask app object if it wouldn't be
needed). An example is given in `config/subscriptions-webhooks.service`.

Note that the proxy endpoint can't reliably access ip-restricted streams if its
IP is different to the frontend's.

## Functional Overview

[rough functional overview diagram](https://viewer.diagrams.net/?highlight=0000ff&edit=_blank&layers=1&nav=1&title=Copy%20of%20youtube-frontend-2020jul19.drawio#R7V1bk5s4Fv41rt19cBcXc3vsS9w7tZlKV7pqJr0vKRlkrAlGDsjddn79SoAwSDKmuw2Y3kmlKkgIgb9zPzpSJubtenefgM3qdxzAaGJowW5i3k0MQ58ZxoT91YJ93uM4s7wjTFBQDDp0PKJfsOjUit4tCmBaG0gwjgja1Dt9HMfQJ7U%2BkCT4pT5siaP6WzcghFLHow8iufdPFJBV3usazqH%2F3xCFK%2F5m3fbyO2vABxe%2FJF2BAL9UusxPE%2FM2wZjkV%2BvdLYwYeByX%2FLn5kbvlhyUwJm0eiKzH%2ByV2vj8tnrz%2Ffr%2F9%2FVEPl1PHzqd5BtG2%2BMXF15I9h4B%2B%2BIZd0lcRBKKvFGYQh%2BzuzcsKEfi4AT67%2F0I5gPatyDqiLZ1eLjAheF00kgIldh3BJb8kmD50p9GrJYqiWxzhhLZjHLP5E7wlKA5v6U%2BEyTfaP9WuLNofgHQFAz4xJoAgHLPbHptIRqYA6xkmBO4qXQVS9xCvIUn2dEhx17XyJwquNfSi%2FXLgAWtWEHZVob9hFp2g4LuwnPpAGnpRUEdNqbv%2F%2FNIf8cOXH0b6ZWk6f3yaJmiqm6cpReGKgwwX7TRxFHDXYD0DilyICxRtW5NQnNkKFE3tDCgq%2Bd1qwe%2BXjqIMYsl1VRBL%2Fjw7iHziCmYwoFqzaOKErHCIYxB9OvTe1FE9jPmMMwXAsPoLErIvTADYElxHOiVU%2F1wzlX6AOuubI%2Fb52bQwDvgIPwJpivy8sxjCpoE7RL7x4fT6iV1nSoW17naVW3f7onGUiCneJj5sgIrrDfqdISRNA%2FNxDMdGnkhgRJXdc906qSicPUrBAPvKgA1GMUkrMz%2BwjgqruXVeswo7d%2BCWfMYD75Sf9nZ24gJxEMkIUwPMnAZAwAKk8KhJYgNSgpMWtugcgmjV0TFdWRRV6szuShBNbwhBpAAm%2B2%2B52Fi8nYuRV7YPgpS19tXWA0wQBQAmReeZJPvtUmq2FFLrIoTU5C4mZ0NTcPyE8bZjNY3vRqhV3oodEWYe0TO9DNnl9cNvvJe%2BpHJDMdYH%2FgoeG31EPfj7CFFeT9p4qplQfF6UHcD%2FEWai8mVL6CzwfGrENAX6Da1GrNmI7fnbpX7W1jRfhthbptmpbd7vo%2Fvd0%2B1zGPz8uf41%2Fwzsr8FUl93jPi1M2ah4aU3Whft2V1TIav6d1%2BzgsYZold7h9CnsiRJd9yIYyxQCDMs2Gu2JON71erAnM4U9OaL1gU9wC5V%2FBk1ua3UkZp4jaXJVlqAzTW6PWZMfpHdWk129R9FtHa4ZFyG69syuMaCu9yCKjv4hmMytMZn7aiarhkE1M3XCSHXPnLNOeFN2SDxR%2BwlqLf%2FQ4qkDV76WyR2u6%2Fl7CofsaLxjaE3juxEKS7JPT3hLtgsasmjs1YZ2j3EYQRrypLL4RBHapBlH88glwtugFxtmOXW0dEOORnRPYcT0zvKLTntbv4I7ELLU%2F82mIp5FbymxRi9ImnqdTy3Nk5BUAdmdN2AOoagretEU%2FHd3lNkhr6XavYww0Tbq2QWjD%2B%2FctgbmNFdIRGrjTES2ZbWzrxa8S894Cn0t5e9u0WZFgW6b7rtPwGJxfPwIzEG5KMcFUbFw1685GDaXQwM5XbAHjuaMUkx5AmQkJmEmJGyMEwsG0njd68GEuANzp%2F2qKG7kjHkZy82GJpTZuM2Mqbte0%2FiOlqe1NuZts42iabpdpH5yNWKjpduC6GsDG602ZU%2BXCKSp1zNzliZXkPUL5Csy6BcFpJDNsTR9WCDNoQ1VzU7pJ%2BzU8CapbdbSPLdJep%2B4yAk9hdafU7h%2BtI5p5gmOCcV1xPbBNGY1aVStePUb1NgDS6P%2BMTIPPHc5Es%2FRFkqaxNp30XG0vKbxHTmORhsV8idcULextQ6hw1cYlzpnkYhjR6hUBAuvKIfqV6WMen3zoJkOyZVcM83eopjeoVH4dp1hkiQnlyt1R2CgI8uVZ0twOCNmq%2FezQdfk1YV1zLKqsify8t9Z0fbbTQAInL4UGn4EmlgUkdnQqlheWvj6%2BEg7rsMwgSFghV4irvSnkjowIEIh2x3mZ9vIaAcDBPkgui5urFEQ5OIGU%2FQLLLKpmGQUbgid17qZWHdsLiphaS5sZ8JcqM62dXnVWNcUoBtdge7ICa9DLcNDgnf70WMuLtWrQFcxeneYy2kun0aLssUoK0VOKAOQbvLtsEu0Y%2FbjPBvRBPWg8NRUO9HOsSdSjZqc07pA1ATDZCiUar%2BoyamNy0OtRImbc0Uiul%2FU5KqkGL7Qju0mwiCgFzEmaElVHNuLLNd4jU1FGk490%2BPJ%2BOvKHdCdEcCVCJDAJYVmxYDOVl7QJtsHznzJn1uYkg9ABEHjWgoqqPYKdUcF2SNbQuIzGrCUqUZ9%2BAx47RkFEH88MSh3Y1cpoFJEnVHAld0zToFNBPYwufqLwq79kwKdAJ%2BRws9Kb%2F41elqYwv7bUjqqtHB6pYUc9FHFk30zjTW%2FZzLwHcVLPHrsZ0JoyDeRDigHhoS9Il%2B6ujLYppj588blSmnCPqlt%2BlSaMCUJBOuJQT9aI6vtehEDFKUts69jo7qtOXWqu%2FJyT1kw3osb5sqB0jaFyT9SwQkYv%2BWRzmJQVBAosT%2BHxKmPoBl65dt1hMW2N621HaXNyVym23bLMBeT%2FhezlYQrj6Dql3C9nrTitCVNN7ujXl36JhRaWWatxvLkeKePnX6uqmZIsoiPECSZ%2B5nvc7phh6699qSGS0qC68J6pD30gqSr2hAlkaHM0bYC%2Fs1bzhQHXp0Bc0NIgiuyDXzJvwp5Z1vQXDnMVfmCnPOpf7CNmLjftvYjE5Q96a9AHMPjDqT0HGD1Q1mkF6GUpG8uBBidO%2BTVWcTRzat2JZVWV0ziqSKQIwoOrbODJqt4i%2FBmJyGWvZ%2FBAkYPFMTiaEN%2BluJNxG7clMfTcFmcGOYy%2B0OHZC%2B75llZTZWiLb7nbkUIO0nzOtPdcz%2BItSvk43iZnZtz5dM3UjYCBNB%2FWD%2Flt%2Flim6IYpul0DWI6yZrSdKqzsyzmBjteb76g%2FsZUN9yrTRyqFUb2cupSZEfznFGL6MKWm3LncDVpbMocwvvO7zEPsuY%2FjMes9jxlr0w57uxO2fv85UEqgNT%2BstOjw9xEwqH9ZTEatvRmf1kY72rn9ZeboGp2Gr7CIECk%2FblnEWKk%2FUj1fqKD7SjyuF052Eq6vWKPyd92fAA77vJyjaHsuBwHJ4UUjyqsclQnDXYUVrXShYw92ec1h67SY3xt0QZrBmO8SDeVgekGxHwkWw0uJ63eaP0u1azpdpHTP33l7COPwcpj0Dk%2FeXIE5nYUgan9a1kwA5RSVDJgDO0h2oboQ6wF1IH3VCufekdrAcqjC%2F%2BPApumoxtPHvHIidJ%2FZNP02c1a%2BY9ilbSo9fwwfqbb4wkcSvRVScW%2F%2FczL8TM9rT8%2Fs1FbVDgkzI6Nk%2BO%2FopRhVP6nqzBbXfmfanzlep1NruTKejWNrADLI9CfzRoIMgeVMHXIRIIVeNLL3x7G71MYwlZeV7HmoiyxfYNPQZuH%2F%2Bgoz7Yc%2Frso89P%2FAA%3D%3D)


## License

This project is licensed under the [GNU Affero General Public License, Version 3].

[GNU Affero General Public License, Version 3]: LICENSE

A copy of hls.js is included; it is available under the [Apache License, Version 2.0].

[Apache License, Version 2.0]: app/static/3rd-party/hls.js/LICENSE

A modified copy of `python-protobuf` is included; it is available under the [MIT License].

[MIT License]: https://opensource.org/license/mit/
