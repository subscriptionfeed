# by default, we listen on port 8080, without HTTPS!
bind = ['0.0.0.0:8080']
# either use this software behind a reverse proxy, or
# configure TLS and use subscriptions-port80.service:
#bind = ['0.0.0.0:443']
#certfile = "/etc/letsencrypt/live/subscriptions.gir.st/fullchain.pem"
#keyfile = "/etc/letsencrypt/live/subscriptions.gir.st/privkey.pem"

accesslog = None
errorlog = "/tmp/frontend21.err"

# Note: We are using the in-memory requests-cache, do not use more than 1
# process, or the cache will be useless! 
workers = 1

worker_class = "gthread"
threads = 40
